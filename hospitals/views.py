from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy

from .models import Hospital


class HospitalList(ListView):
    model = Hospital
    template_name = 'hospitals/list.html'
    context_object_name = 'hospitals'


class HospitalCreate(CreateView):
    model = Hospital
    template_name = 'hospitals/create.html'
    fields = '__all__'
    context_object_name = 'hospital'
    success_url = reverse_lazy('hospital-list')


class HospitalUpdate(UpdateView):
    model = Hospital
    template_name = 'hospitals/update.html'
    fields = '__all__'
    context_object_name = 'hospital'
    success_url = reverse_lazy('hospital-list')


class HospitalDelete(DeleteView):
    model = Hospital
    template_name = 'hospitals/delete.html'
    success_url = reverse_lazy('hospital-list')
