from django.utils.translation import gettext_lazy as _
from django.db import models


class Hospital(models.Model):
    code = models.CharField(_("Hospital code"), max_length=16, primary_key=True)
    name = models.CharField(_("Hospital full name"), max_length=255)
    address = models.TextField(_("Hospital address"), blank=True)
    internal = models.BooleanField(_("Internal medicine"), default=False)
    neurology = models.BooleanField(_("Neurology"), default=False)
    surgery = models.BooleanField(_("Surgery"), default=False)
    gynecology = models.BooleanField(_("Gynecology"), default=False)
    laryngology = models.BooleanField(_("Laryngology"), default=False)
    orthopedics = models.BooleanField(_("Orthopedics"), default=False)
    ophthalmology = models.BooleanField(_("Ophthalmology"), default=False)
    psychiatry = models.BooleanField(_("Psychiatry"), default=False)
    neonatology = models.BooleanField(_("Neonatology"), default=False)

    def __str__(self):
        return self.name
