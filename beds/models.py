from django.utils.translation import gettext_lazy as _
from django.db import models

from hospitals.models import Hospital


class Bed(models.Model):
    code = models.CharField(_("Bed code"), max_length=16, primary_key=True)
    hospital = models.ForeignKey(Hospital, verbose_name=_("Hospital"))
    respirator = models.BooleanField(_("Respirator"), default=False)
    oxygen = models.BooleanField(_("Oxygen"), default=False)
    male = models.BooleanField(_("Male"), default=True)
    female = models.BooleanField(_("Female"), default=True)
