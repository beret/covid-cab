from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy

from .models import Bed


class BedList(ListView):
    model = Bed
    template_name = 'beds/list.html'
    context_object_name = 'beds'


class BedCreate(CreateView):
    model = Bed
    template_name = 'beds/create.html'
    fields = '__all__'
    context_object_name = 'bed'
    success_url = reverse_lazy('bed-list')


class BedUpdate(UpdateView):
    model = Bed
    template_name = 'beds/update.html'
    fields = '__all__'
    context_object_name = 'bed'
    success_url = reverse_lazy('bed-list')


class BedDelete(DeleteView):
    model = Bed
    template_name = 'beds/delete.html'
    success_url = reverse_lazy('bed-list')
