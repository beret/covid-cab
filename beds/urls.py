from django.urls import path
from . import views

urlpatterns = [
    path('create', views.HospitalCreate.as_view(), name='hospital-create'),
    path('<int:pk>/update', views.HospitalUpdate.as_view(), name='hospital-update'),
    path('<int:pk>/delete', views.HospitalDelete.as_view(), name='hospital-delete'),
    path('', views.HospitalList.as_view(), name='hospital-list'),
]
