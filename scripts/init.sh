#!/bin/sh

DONE=/usr/src/app/done
if [[ ! -f $DONE ]];
then
    # wait for postgres
    python wait_for_postgresql.py
    python manage.py migrate
    touch $DONE # to unblock cron task
fi