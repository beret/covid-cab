# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

from config import config

LANGUAGE_CODE = config.LANGUAGE_CODE

TIME_ZONE = config.TZ

USE_I18N = True

USE_L10N = True

USE_TZ = True
