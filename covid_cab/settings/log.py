import sys
import os
from config import get_path

LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')

LOGGING = {
    'version':                  1,
    'disable_existing_loggers': False,
    'handlers':                 {
        'console': {
            'level':  LOG_LEVEL,
            'class':  'logging.StreamHandler',
            'stream': sys.stdout
        },
        'file':    {
            'level':    LOG_LEVEL,
            'class':    'logging.FileHandler',
            'filename': get_path('output', 'django.log'),
        }
    },
    'loggers':                  {
        'django.beret_utils.autoreload': {
            'level': 'INFO',
            # ...
        },
        'django':                  {
            'handlers':  ['console', 'file'],
            'propagate': True,
            'level':     'INFO',
        },
        '':                        {
            'handlers': ['console', 'file'],
            'level':    LOG_LEVEL,
        }
    }
}
