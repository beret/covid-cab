from .base import *
from .templates import *
from .hosts import *
from .db import *
from .auth import *
from .static import *
from .i18n import *
from .log import *
from .rest import *

INSTALLED_APPS = DJANGO_APPS + EXTERNAL_APPS + PROJECT_APPS
