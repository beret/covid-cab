REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',  # simple token auth
    ],
}

SWAGGER_SETTINGS = {
    'USE_SESSION_AUTH':     False,
    'DOC_EXPANSION':        'list',
    'APIS_SORTER':          'alpha',
    'SECURITY_DEFINITIONS': None,
}
