# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

from config import config

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    config.STATICFILES_DIRS,
    config.WEBPACK_STATIC,
]

WEBPACK_LOADER = {
    'MANIFEST_FILE': config.MANIFEST_FILE,
}
