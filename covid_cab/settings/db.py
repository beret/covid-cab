from config import get_path, config

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

# noinspection PyUnresolvedReferences
if config.POSTGRES_DB:
    from .postgres_db import DATABASES
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': get_path('db.sqlite3'),
        }
    }
